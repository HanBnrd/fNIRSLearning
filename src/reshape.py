"""
This script reshapes the preprocessed data for the PyTorch data loader
"""

import numpy as np


PILOTS = ['p02', 'p03', 'p04', 'p05', 'p06', 'p07', 'p08', 'p09', 'p11']
TASKS = ['task1', 'task2', 'task3']
ISA_2_LABELS = [[0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0],
                [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0]]
ISA_3_LABELS = [[0, 0, 0, 1, 2, 2, 2, 2, 1, 1, 0, 0],
                [0, 0, 1, 1, 1, 2, 2, 2, 2, 2, 2, 1],
                [0, 0, 1, 1, 1, 2, 2, 1, 2, 1, 1, 0]]
WINDOW_SIZE = 20  # nb of samples: 10 sec x 2 Hz


def reshape(processed_reader, task, data_writer, label_writer, n_classes):
    """
    Reshape data for the PyTorch data loader
    """
    # Go to the beginning of the file
    processed_reader.seek(0)

    # Skip headers and delay 5 sec
    for i in range(11):
        processed_reader.readline()

    # Get coordinates and labels
    for i in range(12):
        coordinates = []
        labels_coordinates = []
        for _ in range(90):
            line = processed_reader.readline()
            x0 = line.split(',')[1:]
            x0 = [float(i) for i in x0]
            coordinates.append(x0)
            if n_classes == 2:
                labels_coordinates.append(ISA_2_LABELS[TASKS.index(task)][i])
            elif n_classes == 3:
                labels_coordinates.append(ISA_3_LABELS[TASKS.index(task)][i])
        for t in range(0, len(coordinates)-(WINDOW_SIZE-1), 2):
            stack = [coordinates[t]]
            for i in range(1, WINDOW_SIZE):
                stack = np.append(stack, [coordinates[t+i]], axis=0)
            if labels_coordinates[t] != -1:
                data_writer.write('#####\n')
                np.savetxt(data_writer, stack, fmt='%-7.2f')
                label_writer.write(str(labels_coordinates[t])+'\n')


def main():
    """
    Note: K-fold cross validation not implemented in this example
    """
    print("Reshaping:")
    # Open files
    train_data_2 = open('../data/train_data_2.txt', 'w')
    train_label_2 = open('../data/train_labels_2.txt', 'w')
    test_data_2 = open('../data/test_data_2.txt', 'w')
    test_label_2 = open('../data/test_labels_2.txt', 'w')
    train_data_3 = open('../data/train_data_3.txt', 'w')
    train_label_3 = open('../data/train_labels_3.txt', 'w')
    test_data_3 = open('../data/test_data_3.txt', 'w')
    test_label_3 = open('../data/test_labels_3.txt', 'w')

    for i, pilot in enumerate(PILOTS):
        print('Pilot: {}'.format(pilot))
        for task in TASKS:
            print('\tTask: {}'.format(task), end='')
            # Reshape data
            navsd_reader = open('../data/'+pilot+'/'+task+'_navsd.csv', 'r')
            if i != len(PILOTS) - 1:
                print(' | train')
                reshape(navsd_reader, task, train_data_2, train_label_2, 2)
                reshape(navsd_reader, task, train_data_3, train_label_3, 3)
            else:
                print(' | test')
                reshape(navsd_reader, task, test_data_2, test_label_2, 2)
                reshape(navsd_reader, task, test_data_3, test_label_3, 3)
            navsd_reader.close()


if __name__ == '__main__':
    main()
