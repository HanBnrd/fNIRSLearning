# Dataset

Data should be stored in this folder.

- pXX folders: 2 Hz fNIRS data for each participant, participants p01 and p10 removed because too noisy (visual inspection)
- optodes.jpg: optode map
